package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Pilot {
	// Attribute
	private String name;
	private String grad;
	private double posX;
	private double posY;

	// Konstruktor
	public Pilot() {
	}

	public Pilot(String name_new, String grad_new, double posX_new, double posY_new) {
		name = name_new;
		grad = grad_new;
		posX = posX_new;
		posY = posY_new;
	}

	// Set-Methoden
	public void setName(String name_new) {
		name = name_new;
	}

	public void setGrad(String grad_new) {
		grad = grad_new;
	}

	public void setPosX(double posX_new) {
		posX = posX_new;
	}

	public void setPosY(double posY_new) {
		posY = posY_new;
	}

	// Get-Methoden
	public String getName() {
		return name;
	}

	public String getGrad() {
		return grad;
	}

	public double getPosX() {
		return posX;
	}

	public double getPosY() {
		return posY;
	}
}
