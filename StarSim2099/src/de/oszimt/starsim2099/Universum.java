package de.oszimt.starsim2099;

import java.util.ArrayList;
import net.slashie.util.Position;

public class Universum {

	// Attribute
	private int universumBreite = 160;
	private int universumHoehe = 50;
	private ArrayList<Position> listSterne = new ArrayList<Position>();
	private char sternShape = '*';

	// Konstruktor
	public Universum() {
	}

	/**
	 * @param width
	 *            Breite des Universums
	 * @param height
	 *            Höhe des Universums
	 */
	public Universum(int width, int height) {
		this.universumBreite = width;
		this.universumHoehe = height;

		// Generiere zufällige Sterne in einer Dichte von ca 5 Sterne pro 10x10,
		// auch über den eigentlichen "Rand" hinaus
		int starDensitySize = 5;
		for (int i = 0; i < (universumBreite / starDensitySize) * (universumHoehe / starDensitySize); i++) {
			listSterne.add(new Position((int) (Math.random() * (universumBreite + 80) - 40),
					(int) (Math.random() * (universumHoehe + 80) - 40)));
		}

	}

	// Methoden

	public int getUniversumBreite() {
		return universumBreite;
	}

	public int getUniversumHoehe() {
		return universumHoehe;
	}

	public Dimension getUniversumDimension() {
		return new Dimension(universumBreite, universumHoehe);
	}

	public ArrayList<Position> getListSterne() {
		return listSterne;
	}

	public void setSternShape(char sternShape) {
		this.sternShape = sternShape;
	}

	public char getSternShape() {
		return this.sternShape;
	}

}
