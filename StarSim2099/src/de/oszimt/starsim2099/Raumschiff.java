package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff {

	// Attribute
	private String typ;
	private int maxKapazitaet;
	private String antrieb;
	private int winkel;
	private double posX;
	private double posY;

	// Konstruktor
	public Raumschiff() {
	}

	public Raumschiff(String typ_new, int maxKapazitaet_new, String antrieb_new, int winkel_new, double posX_new,
			double posY_new) {
		typ = typ_new;
		maxKapazitaet = maxKapazitaet_new;
		antrieb = antrieb_new;
		winkel = winkel_new;
		posX = posX_new;
		posY = posY_new;
	}

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { { '\0', '\0', '_', '\0', '\0' }, { '\0', '/', 'X', '\\', '\0' },
				{ '\0', '{', 'X', '}', '\0' }, { '\0', '{', 'X', '}', '\0' }, { '/', '_', '_', '_', '\\' }, };
		return raumschiffShape;
	}

	// Getter und Setter
	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public int getMaxLadekapazitaet() {
		return maxKapazitaet;
	}

	public void setMaxLadekapazitaet(int maxKapazitaet) {
		this.maxKapazitaet = maxKapazitaet;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public int getWinkel() {
		return winkel;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

}
