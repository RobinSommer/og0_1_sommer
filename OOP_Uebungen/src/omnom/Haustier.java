package omnom;

public class Haustier {
	String name;

	private int hunger;
	private int muede;
	private int gesund;
	private int zufrieden;

	// Methoden
	public void fuettern(int hunger_plus) {
		hunger += hunger_plus;
		if (hunger > 100) {
			hunger_plus = 100;
		}
	}

	public void schlafen(int muede_plus) {
		muede += muede_plus;
		if (muede > 100) {
			muede = 100;
		}
	}

	public void spielen(int zufrieden_plus) {
		zufrieden += zufrieden_plus;
		if (zufrieden > 100) {
			zufrieden_plus = 100;
		}
	}

	public void heilen() {
		gesund = 100;
	}

	// Konstruktor
	public Haustier() {
	}

	public Haustier(String name_new) {
		name = name_new;
		hunger = 100;
		muede = 100;
		gesund = 100;
		zufrieden = 100;
	}

	// Getter und Setter
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (!(hunger < 0 && hunger > 100)) {
			this.hunger = hunger;
		}
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (!(hunger < 0 && hunger > 100)) {
			this.muede = muede;
		}

	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (!(hunger < 0 && hunger > 100)) {
			this.gesund = gesund;
		}
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (!(hunger < 0 && hunger > 100)) {
			this.zufrieden = zufrieden;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name_new) {
		name = name_new;
	}
}
