package IT1;

import java.util.Scanner;

public class OG0_KontrollstrukturenAufgabe2_RobinSommer {
	public static void main(String[] args) {

		System.out.println("- Sum -");
		System.out.println("");
		System.out.print("Tragen sie bitte den Grenzwert ein: ");

		Scanner scan = new Scanner(System.in);

		int grenze = scan.nextInt();

		int addierer = 2;
		int summe = 0;

		while (summe < grenze) {
			summe += addierer;
			System.out.println("Summe: " + summe);
			addierer += 2;
			System.out.println("Addierer: " + addierer);
		}

		System.out.println("");
		System.out.println("Summe: " + summe);

		scan.close();
	}
}
