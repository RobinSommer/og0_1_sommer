package IT1;

import java.util.Scanner;

public class OG0_KontrollstrukturenAufgabe1_RobinSommer 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);

		int preis;
		int anzahl;
		int lieferungskosten = 10;

		System.out.println("--- Rechnung ---");
		System.out.println("");
		System.out.print("Preis pro Maus: ");

		preis = scan.nextInt();

		System.out.print("Anzahl der M�use: ");

		anzahl = scan.nextInt();

		if (anzahl > 10) {
			lieferungskosten = 0;
		}

		int preisMaeuse = anzahl * preis;

		float mwst = (float) preisMaeuse * 0.09f;

		float endpreis = mwst + preisMaeuse + lieferungskosten;

		System.out.println("");
		System.out.println("-----------------------------");
		System.out.println("");
		System.out.println("-Rechnung -");
		System.out.println("");
		System.out.println("Anzahl der M�use: " + anzahl);
		System.out.println("Preis pro Maus: " + preis + "�");
		System.out.println("");
		System.out.println("Betrag: " + preisMaeuse + "�");
		System.out.println("");
		System.out.println(" + Mehrwertsteuern: " + mwst + "�");
		System.out.println(" + Lieferkosten: " + lieferungskosten + "�");
		System.out.println("");
		System.out.println("Endpreis: " + endpreis + "�");

		scan.close();
	}
}
