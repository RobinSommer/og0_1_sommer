package IT1;

import java.util.Scanner;

public class OG0_KontrollstrukturenAufgabe4_RobinSommer {

	public static void main(String[] args) {
		System.out.println("- Primzahl - ");
		System.out.println("");
		System.out.print("Geben Sie eine Zahl ein: ");

		Scanner scan = new Scanner(System.in);

		long zahl = scan.nextLong();
		double rest = 0;
		boolean primzahl = true;

		int i = 2;
		while (i > (zahl / 2)) {
			rest = zahl % i;
			
			if (rest == 0)
			{
				System.out.println("Ihre Zahl ist keine Primzahl");
				primzahl = false;
			}
			i++;
		}
		
		if (primzahl == true)
		{
			System.out.println("Ihre Zahl ist eine Primzahl");
		}
		
		scan.close();
	}
}
