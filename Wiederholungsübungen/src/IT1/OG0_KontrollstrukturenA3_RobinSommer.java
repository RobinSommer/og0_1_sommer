package IT1;

import java.util.Scanner;

public class OG0_KontrollstrukturenA3_RobinSommer {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);

		System.out.println("- BMI -");

		System.out.println("");
		System.out.print("Bitte geben Sie ihr Geschlecht an [m/w]: ");

		String geschlecht = scan.next();

		System.out.print("Bitte geben Sie ihre K�rpergr��e in cm an: ");

		int groe�e = scan.nextInt();

		System.out.print("Bitte geben Sie ihr Gewicht in kg an: ");

		int gewicht = scan.nextInt();

		double groe�eInM = (groe�e * groe�e) / 10000;
		double bmi = gewicht / groe�eInM;

		System.out.println("");
		System.out.println("BMI: " + bmi);
		System.out.println("");

		if (geschlecht.equals("m"))
		{
			if (bmi<20) {System.out.println("Sie sind Untergewichtig");}
			if (bmi>=20 && bmi<=25) {System.out.println("Sie sind Normalgewichtig");}
			if (bmi > 25) {System.out.println("Sie sind �bergewichtig");}
		}
		if (geschlecht.equals("w"))
		{
			if (bmi<19) {System.out.println("Sie sind Untergewichtig");}
			if (bmi>=19 && bmi<=24) {System.out.println("Sie sind Normalgewichtig");}
			if (bmi > 24) {System.out.println("Sie sind �bergewichtig");}
		}
		
		scan.close();
	}

}
