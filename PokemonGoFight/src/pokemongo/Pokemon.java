package pokemongo;

import java.io.*;
import java.time.*;

public class Pokemon {
	// Attribute
	private int nummer;
	private String name;
	private File bild;
	private int wp;
	private int kp;
	private int maxkp;
	private boolean favorit;
	private String typ;
	private double gewicht;
	private double groesse;
	private int bonbons;
	private LocalDate funddatum;

	// ArrayList - mehr als 2 Attacken

	// Konstruktor
	public Pokemon() {
	}

	public Pokemon(int nummer, String name, File bild, int wp, int kp, int maxkp, boolean favorit, String typ,
			double gewicht, double groesse, int bonbons, LocalDate funddatum) {
		this.nummer = nummer;
		this.name = name;
		this.bild = bild;
		this.wp = wp;
		this.kp = kp;
		this.maxkp = maxkp;
		this.favorit = favorit;
		this.typ = typ;
		this.gewicht = gewicht;
		this.groesse = groesse;
		this.bonbons = bonbons;
		this.funddatum = funddatum;
	}

	// Getter und Setter
	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public File getBild() {
		return bild;
	}

	public void setBild(File bild) {
		this.bild = bild;
	}

	public int getWp() {
		return wp;
	}

	public void setWp(int wp) {
		this.wp = wp;
	}

	public int getKp() {
		return kp;
	}

	public void setKp(int kp) {
		this.kp = kp;
	}

	public int getMaxkp() {
		return maxkp;
	}

	public void setMaxkp(int maxkp) {
		this.maxkp = maxkp;
	}

	public boolean isFavorit() {
		return favorit;
	}

	public void setFavorit(boolean favorit) {
		this.favorit = favorit;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public double getGewicht() {
		return gewicht;
	}

	public void setGewicht(double gewicht) {
		this.gewicht = gewicht;
	}

	public double getGroesse() {
		return groesse;
	}

	public void setGroesse(double groesse) {
		this.groesse = groesse;
	}

	public int getBonbons() {
		return bonbons;
	}

	public void setBonbons(int bonbons) {
		this.bonbons = bonbons;
	}

	public LocalDate getFunddatum() {
		return funddatum;
	}

	public void setFunddatum(LocalDate funddatum) {
		this.funddatum = funddatum;
	}

	@Override
	public String toString() {
		return "Pokemon [nummer=" + nummer + ", name=" + name + ", bild=" + bild + ", wp=" + wp + ", kp=" + kp
				+ ", maxkp=" + maxkp + ", favorit=" + favorit + ", typ=" + typ + ", gewicht=" + gewicht + ", groesse="
				+ groesse + ", bonbons=" + bonbons + ", funddatum=" + funddatum + "]";
	}
	
	
}
