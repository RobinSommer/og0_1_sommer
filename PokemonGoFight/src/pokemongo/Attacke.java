package pokemongo;

public class Attacke {
	private String attackenname;
	private int schaden;

	public Attacke(String attackenname, int schaden) {
		this.attackenname = attackenname;
		this.schaden = schaden;
	}

	public void setAttackenname(String attackenname) {
		this.attackenname = attackenname;
	}

	public String getAttackenname() {
		return attackenname;
	}

	public void setschaden(int schaden) {
		this.schaden = schaden;
	}

	public int getschaden() {
		return schaden;
	}

	@Override
	public String toString() {
		return "Attacke [attackenname=" + attackenname + ", schaden=" + schaden + "]";
	}
}