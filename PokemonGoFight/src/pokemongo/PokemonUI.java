package pokemongo;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import java.awt.Rectangle;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.CardLayout;
import java.awt.Font;
import javax.swing.JInternalFrame;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Toolkit;
import javax.swing.UIManager;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PokemonUI extends JFrame {
	boolean favorit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PokemonUI frame = new PokemonUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PokemonUI() {
		getContentPane().setBackground(Color.GRAY);
		setResizable(true);
		setSize(new Dimension(768, 1024));
		setBackground(new Color(0, 255, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(3, 7));

		JPanel panel = new JPanel();
		panel.setBackground(Color.GREEN);
		panel.setPreferredSize(new Dimension(0, 350));
		getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(10, 75));
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));

		JPanel panel_4 = new JPanel();
		panel_4.setPreferredSize(new Dimension(8, 35));
		panel_1.add(panel_4, BorderLayout.SOUTH);
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 0));

		JLabel lblNewLabel = new JLabel("WP");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_4.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("311");
		lblNewLabel_1.setVerticalTextPosition(SwingConstants.TOP);
		lblNewLabel_1.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 34));
		panel_4.add(lblNewLabel_1);

		JPanel panel_5 = new JPanel();
		panel_5.setPreferredSize(new Dimension(10, 50));
		panel_1.add(panel_5, BorderLayout.NORTH);

		JButton btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (favorit == false) {
					btnNewButton.setIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/starGold.png")));
					favorit = true;
				} else {
					btnNewButton.setIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/starBlack.png")));
					favorit = false;
				}
			}
		});
		btnNewButton.setBorderPainted(false);
		btnNewButton.setContentAreaFilled(false);
		btnNewButton.setIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/starBlack.png")));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		panel_5.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		btnNewButton.setPreferredSize(new Dimension(35, 35));
		panel_5.add(btnNewButton);

		JPanel panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel panel_3 = new JPanel();
		panel_3.setPreferredSize(new Dimension(250, 250));
		panel_2.add(panel_3);

		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblNewLabel_2.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setIconTextGap(1);
		lblNewLabel_2.setIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/glumanda2.png")));
		lblNewLabel_2.setPreferredSize(new Dimension(250, 250));
		panel_3.add(lblNewLabel_2);

		JPanel panel_6 = new JPanel();
		panel_6.setForeground(Color.LIGHT_GRAY);
		panel_6.setBackground(Color.WHITE);
		panel_6.setPreferredSize(new Dimension(10, 100));
		getContentPane().add(panel_6, BorderLayout.CENTER);
		panel_6.setLayout(new BorderLayout(0, 0));

		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		panel_7.setPreferredSize(new Dimension(10, 150));
		panel_6.add(panel_7, BorderLayout.NORTH);
		panel_7.setLayout(new BorderLayout(0, 0));

		JPanel panel_8 = new JPanel();
		panel_8.setBackground(Color.WHITE);
		panel_8.setForeground(Color.BLACK);
		panel_7.add(panel_8, BorderLayout.NORTH);
		panel_8.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblNewLabel_3 = new JLabel("Glumanda");
		lblNewLabel_3.setFont(new Font("Source Serif Pro Light", Font.PLAIN, 40));
		panel_8.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("\r\n");
		JTextField tfName = new JTextField("", 40);
		lblNewLabel_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				lblNewLabel_3.setText("");

				panel_8.add(tfName);
				
			}
		});
		
		tfName.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
               lblNewLabel_3.setText(tfName.getText());
               panel_8.remove(tfName);
            }
        });
		
		lblNewLabel_4.setAlignmentY(Component.TOP_ALIGNMENT);
		lblNewLabel_4.setHorizontalTextPosition(SwingConstants.CENTER);
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setPreferredSize(new Dimension(40, 40));
		lblNewLabel_4.setFont(lblNewLabel_4.getFont().deriveFont(1f));
		lblNewLabel_4.setIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/stift.png")));
		panel_8.add(lblNewLabel_4);

		JPanel panel_9 = new JPanel();
		panel_9.setMinimumSize(new Dimension(10, 5));
		panel_9.setPreferredSize(new Dimension(10, 5));
		panel_9.setBackground(Color.WHITE);
		panel_7.add(panel_9, BorderLayout.CENTER);
		panel_9.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JProgressBar progressBar = new JProgressBar();
		progressBar.setMaximum(52);
		progressBar.setForeground(Color.GREEN);
		progressBar.setValue(52);
		panel_9.add(progressBar);

		JPanel panel_10 = new JPanel();
		panel_10.setBackground(Color.WHITE);
		panel_9.add(panel_10);

		JLabel lblNewLabel_5 = new JLabel("KP");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_10.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("52");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_10.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("/");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_10.add(lblNewLabel_7);

		JLabel lblNewLabel_8 = new JLabel("52");
		lblNewLabel_8.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_10.add(lblNewLabel_8);

		JPanel panel_11 = new JPanel();
		panel_11.setBackground(Color.WHITE);
		panel_11.setPreferredSize(new Dimension(10, 40));
		panel_7.add(panel_11, BorderLayout.SOUTH);
		panel_11.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel panel_12 = new JPanel();
		panel_12.setPreferredSize(new Dimension(150, 40));
		panel_11.add(panel_12);
		panel_12.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblNewLabel_9 = new JLabel("Typ:");
		lblNewLabel_9.setForeground(Color.GRAY);
		lblNewLabel_9.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_12.add(lblNewLabel_9);

		JLabel lblNewLabel_10 = new JLabel("Feuer");
		lblNewLabel_10.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_12.add(lblNewLabel_10);

		JPanel panel_13 = new JPanel();
		panel_13.setPreferredSize(new Dimension(150, 40));
		panel_11.add(panel_13);

		JLabel lblNewLabel_11 = new JLabel("Gewicht:");
		lblNewLabel_11.setForeground(Color.GRAY);
		lblNewLabel_11.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_13.add(lblNewLabel_11);

		JLabel lblNewLabel_12 = new JLabel("8,5");
		lblNewLabel_12.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_13.add(lblNewLabel_12);

		JLabel lblNewLabel_13 = new JLabel("kg");
		panel_13.add(lblNewLabel_13);

		JPanel panel_14 = new JPanel();
		panel_14.setPreferredSize(new Dimension(150, 40));
		panel_11.add(panel_14);

		JLabel lblNewLabel_14 = new JLabel("Gr\u00F6\u00DFe:");
		lblNewLabel_14.setForeground(Color.GRAY);
		lblNewLabel_14.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_14.add(lblNewLabel_14);

		JLabel lblNewLabel_15 = new JLabel("0,6");
		lblNewLabel_15.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_14.add(lblNewLabel_15);

		JLabel lblNewLabel_16 = new JLabel("m");
		panel_14.add(lblNewLabel_16);

		JPanel panel_15 = new JPanel();
		panel_15.setBackground(Color.GRAY);
		panel_6.add(panel_15, BorderLayout.CENTER);
		panel_15.setLayout(new BorderLayout(0, 0));

		JPanel panel_16 = new JPanel();
		panel_16.setBackground(Color.WHITE);
		panel_16.setPreferredSize(new Dimension(10, 150));
		panel_15.add(panel_16, BorderLayout.NORTH);
		panel_16.setLayout(new BoxLayout(panel_16, BoxLayout.Y_AXIS));

		JPanel panel_17 = new JPanel();
		panel_17.setPreferredSize(new Dimension(10, 40));
		panel_17.setBackground(Color.WHITE);
		panel_16.add(panel_17);

		JPanel panel_19 = new JPanel();
		panel_19.setAlignmentY(Component.TOP_ALIGNMENT);
		panel_19.setPreferredSize(new Dimension(250, 60));
		panel_17.add(panel_19);
		panel_19.setLayout(new BoxLayout(panel_19, BoxLayout.Y_AXIS));

		JPanel panel_21 = new JPanel();
		panel_21.setPreferredSize(new Dimension(10, 30));
		panel_19.add(panel_21);

		JLabel lblNewLabel_17 = new JLabel("66726");
		lblNewLabel_17.setFont(new Font("Tahoma", Font.PLAIN, 25));
		panel_21.add(lblNewLabel_17);

		JPanel panel_22 = new JPanel();
		panel_22.setPreferredSize(new Dimension(10, 25));
		panel_19.add(panel_22);

		JLabel lblNewLabel_18 = new JLabel("Sternenstaub");
		panel_22.add(lblNewLabel_18);

		JPanel panel_20 = new JPanel();
		panel_20.setPreferredSize(new Dimension(250, 60));
		panel_17.add(panel_20);
		panel_20.setLayout(new BoxLayout(panel_20, BoxLayout.Y_AXIS));

		JPanel panel_23 = new JPanel();
		panel_23.setPreferredSize(new Dimension(10, 30));
		panel_23.setMinimumSize(new Dimension(10, 30));
		panel_20.add(panel_23);

		JLabel lblNewLabel_17_1 = new JLabel("26");
		lblNewLabel_17_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		panel_23.add(lblNewLabel_17_1);

		JPanel panel_24 = new JPanel();
		panel_24.setPreferredSize(new Dimension(10, 25));
		panel_20.add(panel_24);

		JLabel lblNewLabel_18_1 = new JLabel("Glumanda-Bonbon");
		panel_24.add(lblNewLabel_18_1);

		JPanel panel_18 = new JPanel();
		panel_18.setPreferredSize(new Dimension(10, 40));
		panel_18.setBackground(Color.WHITE);
		panel_16.add(panel_18);
		panel_18.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));

		JButton btnNewButton_1 = new JButton("Power-Up");
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.setBackground(Color.GREEN);
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_1.setPreferredSize(new Dimension(150, 30));
		panel_18.add(btnNewButton_1);

		JLabel lblNewLabel_19 = new JLabel("600");
		lblNewLabel_19.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel_18.add(lblNewLabel_19);

		JLabel lblNewLabel_21 = new JLabel("Ste.");
		panel_18.add(lblNewLabel_21);

		JLabel lblNewLabel_20 = new JLabel("1");
		lblNewLabel_20.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel_18.add(lblNewLabel_20);

		JLabel lblNewLabel_22 = new JLabel("Bon.");
		panel_18.add(lblNewLabel_22);

		JPanel panel_25 = new JPanel();
		panel_25.setPreferredSize(new Dimension(10, 320));
		panel_15.add(panel_25, BorderLayout.SOUTH);
		panel_25.setLayout(new BoxLayout(panel_25, BoxLayout.Y_AXIS));

		JPanel panel_26 = new JPanel();
		panel_26.setPreferredSize(new Dimension(10, 60));
		panel_25.add(panel_26);
		panel_26.setLayout(new BoxLayout(panel_26, BoxLayout.Y_AXIS));

		JPanel panel_33 = new JPanel();
		panel_26.add(panel_33);
		panel_33.setLayout(new BoxLayout(panel_33, BoxLayout.Y_AXIS));

		JPanel panel_30 = new JPanel();
		panel_33.add(panel_30);
		panel_30.setLayout(new BoxLayout(panel_30, BoxLayout.X_AXIS));

		JPanel panel_32 = new JPanel();
		panel_32.setFont(new Font("Tahoma", Font.PLAIN, 20));
		FlowLayout flowLayout = (FlowLayout) panel_32.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel_30.add(panel_32);

		JLabel lblNewLabel_23 = new JLabel("Kratzer");
		lblNewLabel_23.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_32.add(lblNewLabel_23);

		JLabel lblNewLabel_29 = new JLabel("(Normal)");
		lblNewLabel_29.setForeground(Color.GRAY);
		lblNewLabel_29.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_32.add(lblNewLabel_29);

		JPanel panel_35 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_35.getLayout();
		flowLayout_1.setAlignment(FlowLayout.RIGHT);
		panel_30.add(panel_35);

		JLabel lblNewLabel_24 = new JLabel("40");
		lblNewLabel_24.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_35.add(lblNewLabel_24);

		JLabel lblNewLabel_30 = new JLabel("Schaden");
		lblNewLabel_30.setForeground(Color.GRAY);
		lblNewLabel_30.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_35.add(lblNewLabel_30);

		JPanel panel_34 = new JPanel();
		panel_26.add(panel_34);
		panel_34.setLayout(new BoxLayout(panel_34, BoxLayout.Y_AXIS));

		JPanel panel_31 = new JPanel();
		panel_34.add(panel_31);
		panel_31.setLayout(new BoxLayout(panel_31, BoxLayout.X_AXIS));

		JPanel panel_36 = new JPanel();
		FlowLayout flowLayout_3 = (FlowLayout) panel_36.getLayout();
		flowLayout_3.setAlignment(FlowLayout.LEFT);
		panel_31.add(panel_36);

		JLabel lblNewLabel_26 = new JLabel("Glut");
		lblNewLabel_26.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_36.add(lblNewLabel_26);

		JLabel lblNewLabel_28 = new JLabel("(Feuer)");
		lblNewLabel_28.setForeground(Color.GRAY);
		lblNewLabel_28.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_36.add(lblNewLabel_28);

		JPanel panel_37 = new JPanel();
		FlowLayout flowLayout_4 = (FlowLayout) panel_37.getLayout();
		flowLayout_4.setAlignment(FlowLayout.RIGHT);
		panel_31.add(panel_37);

		JLabel lblNewLabel_27 = new JLabel("40");
		lblNewLabel_27.setFont(new Font("Tahoma", Font.PLAIN, 20));
		panel_37.add(lblNewLabel_27);

		JLabel lblNewLabel_31 = new JLabel("Schaden");
		lblNewLabel_31.setForeground(Color.GRAY);
		lblNewLabel_31.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_37.add(lblNewLabel_31);

		JPanel panel_39 = new JPanel();
		panel_39.setSize(new Dimension(0, 1));
		panel_39.setMinimumSize(new Dimension(10, 1));
		panel_39.setAlignmentY(Component.TOP_ALIGNMENT);
		FlowLayout flowLayout_2 = (FlowLayout) panel_39.getLayout();
		flowLayout_2.setHgap(1);
		flowLayout_2.setVgap(1);
		panel_39.setBackground(Color.GRAY);
		panel_39.setPreferredSize(new Dimension(10, 1));
		panel_25.add(panel_39);

		JPanel panel_27 = new JPanel();
		panel_27.setPreferredSize(new Dimension(10, 1));
		panel_25.add(panel_27);

		JLabel lblNewLabel_25 = new JLabel("06.05.2022");
		panel_27.add(lblNewLabel_25);

		JPanel panel_28 = new JPanel();
		panel_28.setPreferredSize(new Dimension(10, 11));
		panel_25.add(panel_28);

		JButton btnNewButton_2 = new JButton("Verschicken");
		btnNewButton_2.setPreferredSize(new Dimension(150, 30));
		btnNewButton_2.setBackground(Color.GREEN);
		btnNewButton_2.setForeground(Color.WHITE);
		panel_28.add(btnNewButton_2);

		JPanel panel_29 = new JPanel();
		panel_29.setPreferredSize(new Dimension(10, 20));
		panel_25.add(panel_29);

		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				System.exit(ABORT);
			}
		});
		btnNewButton_4.setPressedIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/pngwing.com2.png")));
		btnNewButton_4.setIcon(new ImageIcon(PokemonUI.class.getResource("/pokemongo/pngwing.com.png")));
		btnNewButton_4.setSize(new Dimension(35, 35));
		btnNewButton_4.setPreferredSize(new Dimension(35, 35));
		btnNewButton_4.setContentAreaFilled(false);
		btnNewButton_4.setBorderPainted(false);
		panel_29.add(btnNewButton_4);
	}
}
