package buch;

public class Buch implements Comparable<Buch> {
	String autor;
	String titel;
	String isbn;
	
	public Buch(String n_autor, String n_titel, String n_isbn)
	{
		autor = n_autor;
		titel = n_titel;
		isbn = n_isbn;
	}
	
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getTitel() {
		return titel;
	}
	public void setTitel(String titel) {
		this.titel = titel;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	public boolean equals(Buch b){
		if (b.getIsbn().equals(this.isbn))
		{
			b.setAutor(this.autor);
			b.setIsbn(this.isbn);
			b.setTitel(this.titel);
			
			System.out.println("Gleiche B�cher! �berschrieben...");
			System.out.println("");
			
			return true;
		}
		return false;
	}
	
	public String toString(){
		return "[ ISBN: "+this.isbn + ", "+ "Autor: "+this.autor + ", " + "Titel: " + this.titel + " ]";
	}
	
	public int compareTo(Buch b){
		// Zahl f�r ISBN1
		String isbn_1 = b.getIsbn();
		String isbn_zahl = "";
		boolean doppelP = false;
		for (int i = 0; i < isbn_1.length(); i++)
		{
			if (doppelP)
			{
				isbn_zahl += isbn_1.charAt(i);
			}
			else
			{
				if (isbn_1.charAt(i) == ':')
				{
					doppelP = true;
				}
			}
		}
		
		// Zahl f�r ISBN2
		String isbn_2 = this.getIsbn();
		String isbn_zahl2 = "";
		boolean doppelP2 = false;
		for (int i = 0; i < isbn_2.length(); i++)
		{
			if (doppelP2)
			{
				isbn_zahl2 += isbn_2.charAt(i);
			}
			else
			{
				if (isbn_2.charAt(i) == ':')
				{
					doppelP2 = true;
				}
			}
		}
		
		if (Integer.valueOf(isbn_zahl) > Integer.valueOf(isbn_zahl2))
		{
			return 1;
		}
		else if (Integer.valueOf(isbn_zahl) > Integer.valueOf(isbn_zahl2))
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
}
