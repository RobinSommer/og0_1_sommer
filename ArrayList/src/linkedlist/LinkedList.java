package linkedlist;

import java.util.*;

public class LinkedList {
	public static void main(String[] args)
	{
		ArrayList<Integer> liste = new ArrayList<Integer>();
		Random ran = new Random();
		
		for (int i = 0; i < 15; i++)
		{
			liste.add(ran.nextInt(100) + 1);
		}
		
		for (int i = 0; i < liste.size(); i++)
		{
			System.out.print(liste.get(i) + ", ");
		}
		
		System.out.println("");
		System.out.println("Size: " + liste.size());
		
		liste.remove(ran.nextInt(liste.size()-1) + 0);
		liste.remove(ran.nextInt(liste.size()-1) + 0);
		liste.remove(ran.nextInt(liste.size()-1) + 0);
		liste.remove(ran.nextInt(liste.size()-1) + 0);
		liste.remove(ran.nextInt(liste.size()-1) + 0);
		
		System.out.println("");
		System.out.println("Zahlen entfernt");
		
		System.out.println("Size: " + liste.size());
		
		System.out.println("");
		for (int i = 0; i < liste.size(); i++)
		{
			System.out.print(liste.get(i) + ", ");
		}
		
		System.out.println("");
		
		liste.clear();
		System.out.println("Liste geleert");
		System.out.println("Size: " + liste.size());
	}
}
