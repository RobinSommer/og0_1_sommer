public class Person implements Comparable<Person>{
	
	private String personalId;
	private String name;
	
	public Person(String personalId, String name) {
		this.personalId = personalId;
		this.name = name;
	}

	
	public String getPersonalId() {
		return personalId;
	}

	public void setPersonalId(String personalId) {
		this.personalId = personalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Person p){
		return p.getPersonalId().equals(this.personalId);
	}
	
	public String toString(){
		return "[ PersonalId: "+this.personalId + ", "+ "Name: "+this.name + " ]";
	}
	
	public int compareTo(Person p){
		return this.personalId.compareTo(p.getPersonalId());
		
	}

}
