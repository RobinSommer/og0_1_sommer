import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Scanner;

public class EinfacheUebung {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> al = new ArrayList<Integer>();
		System.out.println("Gib die Länge der Liste ein, ACHTUNG TRICK");
		int laenge = sc.nextInt();
		
		for (int i = 0; i < laenge + 1; i++) {
			al.add(laenge - i);
		} // end of for
		
		ListIterator<Integer> li = al.listIterator();
		while (li.hasNext()) {
			System.out.println(li.next());
		} // end of while
		
		System.out.println();
		al.remove(Integer.valueOf(2));
		for (int ausgabe : al) {
			System.out.println(ausgabe);
		} // end of for
	} // end of main

} // end of class ArrayLTest
