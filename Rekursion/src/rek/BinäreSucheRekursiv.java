package rek;

public class BinäreSucheRekursiv
{
	public int binareRekursiv(int[] liste, int von, int bis, int suchendeZahl)
	{
		if (bis <= von)
		{
			return -1;
		}

		int mitte = (von + bis) >>> 1;
		int mitteWert = liste[mitte];

		if (suchendeZahl == mitteWert)
		{
			return mitte;
		}
		else if (suchendeZahl < mitteWert)
		{
			return binareRekursiv(liste, von, mitte, suchendeZahl);
		}
		else
		{
			return binareRekursiv(liste, mitte + 1, bis, suchendeZahl);
		}
	}
}
