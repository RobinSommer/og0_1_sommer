package Samer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.*;
import java.util.Arrays;
import java.util.Comparator;

public class Musikvoting {
	private static String url = "jdbc:mysql://localhost/musikvoting";
	private static String user = "root";
	private static String password = "";

	private static ArrayList<Song> songZwischenspeicher = new ArrayList<Song>();
	public static LinkedList accounts = new LinkedList();

	static public void accountLoeschen(String name) {
		accounts.remove(name);
	}

	// Songs in Database und ArrayList hinzuf�gen
	public static void addSong(Song s) {
		final String SQL_INSERT = "INSERT INTO t_musik_id (Musik_ID, Titel, Interpret, Genre) VALUES (?,?,?,?)";

		try (Connection conn = DriverManager.getConnection(url, user, password);
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {

			String query = "select count(*) from t_musik_id";
			ResultSet rs = preparedStatement.executeQuery(query);
			rs.next();
			int count = rs.getInt(1);

			preparedStatement.setInt(1, count);
			preparedStatement.setString(2, s.getInterpret());
			preparedStatement.setString(3, s.getTitel());
			preparedStatement.setString(4, s.getGenre());

			int row = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		loadSongs();
	}

	public static ArrayList<Song> getSongZwischenspeicher() {
		return songZwischenspeicher;
	}

	// Songs l�schen
	public static void clearTable(String table_name) {
		String clearTable = "DELETE FROM " + table_name + ";";
		try (Connection conn = DriverManager.getConnection(url, user, password);
				PreparedStatement preparedStatement = conn.prepareStatement(clearTable)) {

			int row = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Songs aus der Database in die ArrayList laden
	public static void loadSongs() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		ArrayList<String> titel = new ArrayList<String>();
		ArrayList<String> interpreten = new ArrayList<String>();
		ArrayList<String> genres = new ArrayList<String>();

		// Arraylist f�llen mit den IDs
		Connection con1 = null;
		Statement statement1 = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con1 = DriverManager.getConnection(url, user, password);
			statement1 = (Statement) con1.createStatement();
			String sql;
			sql = "select * from musikvoting.t_musik_id";
			ResultSet resultSet = statement1.executeQuery(sql);
			while (resultSet.next()) {
				ArrayList<Integer> inner = new ArrayList<Integer>();
				inner.add(resultSet.getInt("Musik_ID"));
				ids.add(inner.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Arraylist f�llen mit den Titeln
		Connection con2 = null;
		Statement statement2 = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con2 = DriverManager.getConnection(url, user, password);
			statement2 = (Statement) con2.createStatement();
			String sql;
			sql = "select * from musikvoting.t_musik_id";
			ResultSet resultSet = statement2.executeQuery(sql);
			while (resultSet.next()) {
				ArrayList<String> inner = new ArrayList<String>();
				inner.add(resultSet.getString("Titel"));
				titel.add(inner.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Arraylist f�llen mit den Interpreten
		Connection con3 = null;
		Statement statement3 = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con3 = DriverManager.getConnection(url, user, password);
			statement3 = (Statement) con3.createStatement();
			String sql;
			sql = "select * from musikvoting.t_musik_id";
			ResultSet resultSet = statement3.executeQuery(sql);
			while (resultSet.next()) {
				ArrayList<String> inner = new ArrayList<String>();
				inner.add(resultSet.getString("Interpret"));
				interpreten.add(inner.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Arraylist f�llen mit den Genren
		Connection con4 = null;
		Statement statement4 = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con4 = DriverManager.getConnection(url, user, password);
			statement4 = (Statement) con4.createStatement();
			String sql;
			sql = "select * from musikvoting.t_musik_id";
			ResultSet resultSet = statement4.executeQuery(sql);
			while (resultSet.next()) {
				ArrayList<String> inner = new ArrayList<String>();
				inner.add(resultSet.getString("Genre"));
				genres.add(inner.get(0));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		songZwischenspeicher.clear();
		for (int i = 0; i < ids.size(); i++) {
			songZwischenspeicher.add(new Song(ids.get(i), titel.get(i), interpreten.get(i), genres.get(i)));
		}
	}

	// Songs aus der Database in einen two-dimensional array laden, f�r die
	// Darstellung im MusikVotenGUI
	public static String[][] getSongsInTDA() {
		loadSongs();
		String[][] songs = new String[songZwischenspeicher.size()][3];

		for (int i = 0; i < songZwischenspeicher.size(); i++) {
			songs[i][0] = songZwischenspeicher.get(i).getTitel();
			songs[i][1] = songZwischenspeicher.get(i).getInterpret();
			songs[i][2] = songZwischenspeicher.get(i).getGenre();
		}

		return songs;
	}

	public static String[][] getSongsInTDAWithVotes() {
		loadSongs();
		String[][] songs = new String[songZwischenspeicher.size()][4];

		for (int i = 0; i < songZwischenspeicher.size(); i++) {
			int count = 0;

			String query = "select count(VoteID) from t_musik_t_g�ste where Musik_ID = "
					+ songZwischenspeicher.get(i).getMusik_id() + ";";
			try (Connection conn = DriverManager.getConnection(url, user, password);
					PreparedStatement preparedStatement = conn.prepareStatement(query)) {

				ResultSet rs = preparedStatement.executeQuery(query);
				rs.next();
				count = rs.getInt(1);

			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}

			songs[i][0] = String.valueOf(count);
			songs[i][1] = songZwischenspeicher.get(i).getTitel();
			songs[i][2] = songZwischenspeicher.get(i).getInterpret();
			songs[i][3] = songZwischenspeicher.get(i).getGenre();
		}

		// Liste nach Votes sortieren
		Arrays.sort(songs, (a, b) -> Integer.compare(Integer.valueOf(b[0]), Integer.valueOf(a[0])));

		return songs;
	}

	public static void addGast(String loginname) {
		final String queryCheck = "SELECT count(*) from t_gast WHERE Benutzername = ?";
		int count = 0;
		try (Connection conn = DriverManager.getConnection(url, user, password);
				PreparedStatement preparedStatement = conn.prepareStatement(queryCheck)) {

			final PreparedStatement ps = conn.prepareStatement(queryCheck);
			ps.setString(1, loginname);
			final ResultSet resultSet = ps.executeQuery();

			if (resultSet.next()) {
				count = resultSet.getInt(1);
			}
		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (count == 0) {
			final String SQL_INSERT = "INSERT INTO t_gast (Benutzername) VALUES (?);";
			try (Connection conn = DriverManager.getConnection(url, user, password);
					PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {
				preparedStatement.setString(1, loginname);
				int row = preparedStatement.executeUpdate();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// Kein Eintrag in die Datenbank
		}
	}

	// Votings speichern
	public static void voteEintragen(String loginname, Song voteSong) {
		addGast(loginname);

		final String SQL_INSERT = "INSERT INTO t_musik_t_g�ste (VoteID, Benutzername, Musik_ID) VALUES (?,?,?);";

		try (Connection conn = DriverManager.getConnection(url, user, password);
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {

			String query = "select count(*) from t_musik_t_g�ste";
			ResultSet rs = preparedStatement.executeQuery(query);
			rs.next();
			int count = rs.getInt(1);

			preparedStatement.setInt(1, count);
			preparedStatement.setString(2, loginname);
			preparedStatement.setInt(3, voteSong.getMusik_id());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		final String SQL_INSERT2 = "SELECT count(VoteID) from t_musik_t_g�ste WHERE Benutzername = '" + loginname
				+ "';";
		try (Connection conn = DriverManager.getConnection(url, user, password);
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {

			ResultSet rs = preparedStatement.executeQuery(SQL_INSERT2);
			rs.next();
			int votes = rs.getInt(1);
			System.out.println(votes);

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		final String sqlInsert3 = "SELECT count(VoteID) from t_musik_t_g�ste WHERE Benutzername = '" + loginname + "';";
		int votes = 0;
		try (Connection conn = DriverManager.getConnection(url, user, password);
				PreparedStatement preparedStatement = conn.prepareStatement(SQL_INSERT)) {

			ResultSet rs = preparedStatement.executeQuery(SQL_INSERT2);
			rs.next();
			votes = rs.getInt(1);
			System.out.println(votes);

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (votes > 5) {
			/*final String sqlInsert4 = "SELECT count(VoteID) from t_musik_t_g�ste WHERE Benutzername = '" + loginname
					+ "';";

			try (Connection conn = DriverManager.getConnection(url, user, password);
					PreparedStatement preparedStatement = conn.prepareStatement(sqlInsert4)) {
				int row = preparedStatement.executeUpdate();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			} catch (Exception e) {
				e.printStackTrace();
			} */

			System.out.println("Mehr als 5 Votes get�tigt!");
		}
	}

	public void deletePlaylist() {
		songZwischenspeicher.clear();

		clearTable("t_gast");
		clearTable("t_musik_id");
		clearTable("t_musik_t_g�ste");
	}
}
