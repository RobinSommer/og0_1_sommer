package Samer;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class SamerGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Musikvoting mcc;
	private JTextField tfdName;
	private JTextField tfdTitel;
	private JTextField tfdBand;
	private JTextField tfdGenre;
	private JLabel lblFehlermeldung;
	private JPanel pnlCenter;
	private JTable tblMusikListe;
	private JTable tblPlayList;
	private String PF_Name;
	private String PF_Titel;
	private JTextField txtTitel;
	private JTextField txtBand;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SamerGUI frame = new SamerGUI();
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public SamerGUI() {
		// Logikschicht
		this.mcc = new Musikvoting();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 450);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 230, 140));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblMusikvoting = new JLabel();
		lblMusikvoting.setHorizontalAlignment(SwingConstants.CENTER);
		lblMusikvoting.setFont(new Font("Algerian", Font.PLAIN, 36));
		lblMusikvoting.setText("JUKEBOX �SOUND OF AXE AND FIRE�");
		lblMusikvoting.setForeground(Color.RED);
		contentPane.add(lblMusikvoting, BorderLayout.NORTH);		

		pnlCenter = new JPanel();
		pnlCenter.setBackground(new Color(240, 230, 140));
		contentPane.add(pnlCenter, BorderLayout.CENTER);
		pnlCenter.setLayout(new CardLayout(0, 0));

		JPanel pnlCardNeuerGast = new JPanel();
		pnlCardNeuerGast.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlCardNeuerGast, "anmelden/registrieren");
		pnlCardNeuerGast.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlCardNeuerGast.setLayout(null);

		JLabel lblName = new JLabel("NAME");
		lblName.setBounds(5, 6, 762, 111);
		lblName.setHorizontalAlignment(SwingConstants.CENTER);
		lblName.setFont(new Font("Algerian", Font.BOLD, 30));
		pnlCardNeuerGast.add(lblName);

		tfdName = new JTextField();
		tfdName.setFont(new Font("Algerian", Font.BOLD, 22));
		tfdName.setBounds(80, 130, 619, 75);
		tfdName.setHorizontalAlignment(SwingConstants.CENTER);
		tfdName.setColumns(10);
		pnlCardNeuerGast.add(tfdName);

		JButton button = new JButton("ANMELDEN/REGISTRIEREN");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (tfdName.getText().equals(""))
				{
					tfdName.setBackground(Color.RED);
				}
				else
				{
					tfdName.setBackground(Color.WHITE);
					mcc.addGast(tfdName.getText());
					CardLayout cl = (CardLayout) (pnlCenter.getLayout());
					btnEintragen_Clicked();
					cl.show(pnlCenter, "Auswahl");
				}
				
			}
		});
		button.setBounds(208, 257, 386, 75);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		pnlCardNeuerGast.add(button);
		
		JPanel pnlAuswahl = new JPanel();
		pnlAuswahl.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlAuswahl, "Auswahl");
		pnlAuswahl.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnLied = new JButton("LIED EINFUEGEN");
		btnLied.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "Lied");
			}
		});
		pnlAuswahl.add(btnLied);
		
		JPanel pnlLied = new JPanel();
		pnlLied.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlLied, "Lied");
		pnlLied.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlMAEV = new JPanel();
		pnlLied.add(pnlMAEV, BorderLayout.SOUTH);
		pnlMAEV.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnMenu = new JButton("MENU");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfdTitel.setText("");
				tfdBand.setText("");
				tfdGenre.setText("");
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "Auswahl");
			}
		});
		pnlMAEV.add(btnMenu);
		
		
		JButton btnAbmelden = new JButton("ABMELDEN");
		btnAbmelden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfdTitel.setText("");
				tfdBand.setText("");
				tfdGenre.setText("");
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.first(pnlCenter);
			}
		});
		
		JButton btnEinfuegen = new JButton("EINFUEGEN");
		btnEinfuegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (!(tfdBand.getText().equals("")) && !(tfdTitel.getText().equals(""))
						&& !(tfdGenre.getText()).equals("")) {

					if (mcc.getSongZwischenspeicher().size() == 0) {
						mcc.addSong(new Song(mcc.getSongZwischenspeicher().size(),
								tfdTitel.getText(), tfdBand.getText(), tfdGenre.getText()));
						System.out.println("Song hinzugefügt!");
					} else {
						for (int i = 0; i <mcc.getSongZwischenspeicher().size(); i++) {
							if (mcc.getSongZwischenspeicher().get(i).getTitel().equals(tfdTitel.getText())
									&& mcc.getSongZwischenspeicher().get(i).getInterpret().equals(tfdBand.getText())) {
								System.out.println("Der Song exisitert bereits!");
								break;
							} else {
								mcc.addSong(new Song(mcc.getSongZwischenspeicher().size(),
										tfdTitel.getText(), tfdBand.getText(), tfdGenre.getText()));
								System.out.println("Song hinzugefügt!");
								break;
							}
						}
					}
				}
				
				btnLied_Clicked();
			}
		});
		pnlMAEV.add(btnEinfuegen);
		pnlMAEV.add(btnAbmelden);
		
		JPanel pnlTBG = new JPanel();
		pnlLied.add(pnlTBG, BorderLayout.CENTER);
		pnlTBG.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlDracheL = new JPanel();
		pnlDracheL.setBackground(Color.WHITE);
		pnlTBG.add(pnlDracheL, BorderLayout.EAST);
		JLabel lblDracheL = new JLabel();
		pnlDracheL.add(lblDracheL);
		lblDracheL.setIcon(new ImageIcon("./src/bilder/DracheL.png"));
		
		JPanel pnlDracheD = new JPanel();
		pnlDracheD.setBackground(Color.WHITE);
		pnlTBG.add(pnlDracheD, BorderLayout.WEST);
		JLabel lblDracheD = new JLabel();
		pnlDracheD.add(lblDracheD);
		lblDracheD.setIcon(new ImageIcon("./src/bilder/DracheD.png"));
		
		JPanel pnlTiBaGe = new JPanel();
		pnlTiBaGe.setBackground(Color.WHITE);
		pnlTBG.add(pnlTiBaGe, BorderLayout.CENTER);
		pnlTiBaGe.setLayout(null);
		
		JLabel lblTitel = new JLabel("TITEL");
		lblTitel.setBounds(30, 100, 38, 16);
		pnlTiBaGe.add(lblTitel);
		lblTitel.setForeground(Color.BLACK);
		tfdTitel = new JTextField();
		tfdTitel.setBounds(100, 97, 116, 22);
		pnlTiBaGe.add(tfdTitel);
		tfdTitel.setColumns(10);
		
		JLabel lblBand = new JLabel("BAND");
		lblBand.setBounds(30, 150, 38, 16);
		pnlTiBaGe.add(lblBand);
		
		tfdBand = new JTextField();
		tfdBand.setBounds(100, 147, 116, 22);
		pnlTiBaGe.add(tfdBand);
		tfdBand.setColumns(10);
		
		JLabel lblGenre = new JLabel("GENRE");
		lblGenre.setBounds(30, 200, 46, 16);
		pnlTiBaGe.add(lblGenre);
		
		tfdGenre = new JTextField();
		tfdGenre.setBounds(100, 197, 116, 22);
		pnlTiBaGe.add(tfdGenre);
		tfdGenre.setColumns(10);
		
		JLabel lblWapen2 = new JLabel();
		lblWapen2.setHorizontalAlignment(SwingConstants.CENTER);
		lblWapen2.setIcon(new ImageIcon("./src/bilder/Stark.png"));
		lblWapen2.setBackground(Color.RED);
		pnlAuswahl.add(lblWapen2);
		
		JButton btnPlaylist = new JButton("PLAYLIST AUSGEBEN");
		btnPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPlayList_Clicked();
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "PlayList");
				mcc.loadSongs();
			}
		});
		pnlAuswahl.add(btnPlaylist);
		
		JLabel lblWapen1 = new JLabel();
		lblWapen1.setHorizontalAlignment(SwingConstants.CENTER);
		lblWapen1.setIcon(new ImageIcon("./src/bilder/Lennister.png"));
		lblWapen1.setBackground(Color.RED);
		pnlAuswahl.add(lblWapen1);
		
		
		
		JButton btnVoten = new JButton("VOTEN");
		btnVoten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mcc.loadSongs();
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "MusikListe");
				btnMusikListe_Clicked();
				
			}
		});
		pnlAuswahl.add(btnVoten);
		
		JLabel lblWapen3 = new JLabel();
		lblWapen3.setHorizontalAlignment(SwingConstants.CENTER);
		lblWapen3.setIcon(new ImageIcon("./src/bilder/Targaryen.png"));
		lblWapen3.setBackground(Color.RED);
		pnlAuswahl.add(lblWapen3);
		
		JPanel pnlMusikListe = new JPanel();
		pnlMusikListe.setBackground(new Color(240, 230, 140));
		pnlCenter.add(pnlMusikListe, "MusikListe");
		pnlMusikListe.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlMVA = new JPanel();
		pnlMusikListe.add(pnlMVA, BorderLayout.SOUTH);
		pnlMVA.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnM = new JButton("MENU");
		pnlMVA.add(btnM);
		btnM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "Auswahl");
			}
		});
		
		JButton btnV = new JButton("VOTEN");
		btnV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnV_Clicked();

				Song voteSong = null;
				mcc.loadSongs();
				ArrayList<Song> l = mcc.getSongZwischenspeicher();
				for (int i = 0; i < l.size(); i++) {
					if (l.get(i).getTitel().equals(txtTitel.getText()) && l.get(i).getInterpret().equals(txtBand.getText()))
					{
						
						voteSong = l.get(i);
					}
				}
				
				mcc.voteEintragen(tfdName.getText(), voteSong);
				
				txtTitel.setText("");
				txtBand.setText("");
			}
		});
		pnlMVA.add(btnV);
		
		JButton btnA = new JButton("ABMELDEN");
		pnlMVA.add(btnA);
		btnA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.first(pnlCenter);
			}
		});
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(240, 230, 140));
		pnlMusikListe.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));
		
		String[] spaltennamen = {"Titel",
                "Interpret",
                "Genre"};
		mcc.loadSongs();
		String[][] data = (mcc.getSongsInTDA());
		JTable tblMusikListe = new JTable(data, spaltennamen);
		tblMusikListe.setFillsViewportHeight(true);
		JScrollPane spnMusikListe = new JScrollPane(tblMusikListe);
		panel.add(spnMusikListe, BorderLayout.WEST);
		spnMusikListe.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JPanel pnlPlayList = new JPanel();
		pnlCenter.add(pnlPlayList, "PlayList");
		pnlPlayList.setLayout(new BorderLayout(0, 0));
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(240, 230, 140));
		pnlPlayList.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		String[] spaltennamen2 = {"Anzahl Votes", 
				"Titel",
                "Interpret",
                "Genre"};
		mcc.loadSongs();
		String[][] playlist = (mcc.getSongsInTDAWithVotes());
		JTable tblPlayList = new JTable(playlist, spaltennamen2);
		tblPlayList.setFillsViewportHeight(true);
		JScrollPane spnPlayList = new JScrollPane(tblPlayList);
		panel_2.add(spnPlayList, BorderLayout.NORTH);
		spnPlayList.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		JPanel pnlVoting = new JPanel();
		panel.add(pnlVoting);
		pnlVoting.setBackground(Color.WHITE);
		pnlVoting.setLayout(null);
		
		JLabel lblGoT = new JLabel("");
		lblGoT.setHorizontalAlignment(SwingConstants.CENTER);
		lblGoT.setBounds(0, 0, 320, 320);
		lblGoT.setIcon(new ImageIcon("./src/bilder/GoT.png"));
		pnlVoting.add(lblGoT);
		
		JLabel lblFrage = new JLabel("Für welches Lied möchtest du voten?");
		lblFrage.setHorizontalAlignment(SwingConstants.CENTER);
		lblFrage.setBounds(37, 307, 258, 23);
		pnlVoting.add(lblFrage);
		
		txtTitel = new JTextField();
		txtTitel.setBounds(73, 251, 186, 23);
		pnlVoting.add(txtTitel);
		txtTitel.setColumns(10);
		
		txtBand = new JTextField();
		txtBand.setColumns(10);
		txtBand.setBounds(73, 285, 186, 23);
		pnlVoting.add(txtBand);
		
		JLabel lblTitelVote = new JLabel("Titel");
		lblTitelVote.setBounds(10, 256, 46, 14);
		pnlVoting.add(lblTitelVote);
		
		JLabel lblBandVote = new JLabel("Band");
		lblBandVote.setBounds(10, 289, 46, 14);
		pnlVoting.add(lblBandVote);
		
		JButton btnAB = new JButton();
		btnAB.setText("ABMELDEN");
		pnlPlayList.add(btnAB, BorderLayout.EAST);
		btnAB.setForeground(new Color(0, 0, 0));
		btnAB.setBackground(Color.WHITE);
		
		JButton btnME = new JButton();
		btnME.setText("    MENU    ");
		btnME.setIcon(new ImageIcon("./src/bilder/PlayList.png"));
		pnlPlayList.add(btnME, BorderLayout.WEST);
		btnME.setForeground(new Color(0, 0, 0));
		btnME.setBackground(Color.WHITE);
		btnME.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.show(pnlCenter, "Auswahl");
			}
		});
		btnAB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CardLayout cl = (CardLayout) (pnlCenter.getLayout());
				cl.first(pnlCenter);
			}
		});
	}

	public void btnMusikListe_Clicked() {
		//tblMusikListe.setModel(new DefaultTableModel(mcc.getMusikliste().getMusik(), MusikListe.SPALTENNAMEN));
		//((CardLayout) pnlCenter.getLayout()).show(pnlCenter, "MusikListe");
	}
	
	public void btnPlayList_Clicked() {
		//tblPlayList.setModel(new DefaultTableModel(mcc.getPlaylist().getPlayList(), PlayList.SPALTENNAMEN));
	}
	
	public void btnEintragen_Clicked() {
		
			/*Gast g = new Gast(tfdName.getText());
			PF_Name = tfdName.getText();
			tfdName.setText("");
			boolean ergebnis = this.mcc.neuerGast(g);*/
		
	}
	
	public void btnLied_Clicked() {
		try {
			Song m = new Song(0, tfdTitel.getText(), tfdBand.getText(), tfdGenre.getText());
			tfdTitel.setText("");
			tfdBand.setText("");
			tfdGenre.setText("");
			//boolean ergebnis = this.mcc.addSong(m);
		} catch (NumberFormatException e) {
			lblFehlermeldung.setText("Fehler");
		}	
	}
	
	public void btnV_Clicked() {
		try {
			//boolean ergebnis = this.mcc.neueVerbindung(PF_Name, PF_Titel);
		} catch (NumberFormatException e) {
			lblFehlermeldung.setText("Fehler");
		}	
	}
}