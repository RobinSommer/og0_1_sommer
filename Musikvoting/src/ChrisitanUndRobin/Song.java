package ChrisitanUndRobin;

public class Song {
	private int musik_id;
	private String titel;
	private String interpret;
	private String genre;

	public Song(int musik_id, String titel, String interpret, String genre) {
		this.musik_id = musik_id;
		this.titel = titel;
		this.interpret = interpret;
		this.genre = genre;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getInterpret() {
		return interpret;
	}

	public void setInterpret(String interpret) {
		this.interpret = interpret;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getMusik_id() {
		return musik_id;
	}

	public void setMusik_id(int musik_id) {
		this.musik_id = musik_id;
	}
}
