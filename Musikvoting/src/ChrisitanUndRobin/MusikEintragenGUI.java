package ChrisitanUndRobin;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class MusikEintragenGUI extends JFrame {

	private static final long serialVersionUID = 6856062116794859623L;
	private JPanel contentPane;
	private JTextField tfdBandname;
	private JTextField tfdTitelname;
	private JTextField tfdGenre;
	private JLabel lblPlatzhalter;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusikEintragenGUI frame = new MusikEintragenGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public MusikEintragenGUI() {
		setTitle("Musikvoting - Songs hinzufügen");
		setBounds(100, 100, 520, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(9, 1, 0, 0));

		JLabel lblMusikvotingDerGruppe = new JLabel("Musikvoting der Gruppe XXX - Musiktitel eintragen");
		lblMusikvotingDerGruppe.setForeground(Color.WHITE);
		lblMusikvotingDerGruppe.setHorizontalAlignment(SwingConstants.CENTER);
		lblMusikvotingDerGruppe.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		contentPane.add(lblMusikvotingDerGruppe);

		JLabel lblBandname = new JLabel("Titel");
		lblBandname.setForeground(Color.WHITE);
		lblBandname.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		contentPane.add(lblBandname);

		tfdBandname = new JTextField();
		contentPane.add(tfdBandname);
		tfdBandname.setColumns(10);
		JLabel lblTitelname = new JLabel("Interpret");
		lblTitelname.setForeground(Color.WHITE);
		lblTitelname.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		contentPane.add(lblTitelname);

		tfdTitelname = new JTextField();
		contentPane.add(tfdTitelname);
		tfdTitelname.setColumns(10);

		JLabel lblGenre = new JLabel("Genre");
		lblGenre.setForeground(Color.WHITE);
		lblGenre.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		contentPane.add(lblGenre);

		tfdGenre = new JTextField();
		contentPane.add(tfdGenre);
		tfdGenre.setColumns(10);

		lblPlatzhalter = new JLabel("");
		contentPane.add(lblPlatzhalter);

		JButton btnMusiktitelHinzufgen = new JButton("Musiktitel hinzuf\u00FCgen");
		btnMusiktitelHinzufgen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Musikvoting musikvoting = new Musikvoting();
				
				

				if (!(tfdBandname.getText().equals("")) && !(tfdTitelname.getText().equals(""))
						&& !(tfdGenre.getText()).equals("")) {

					if (musikvoting.getSongZwischenspeicher().size() == 0) {
						musikvoting.addSong(new Song(musikvoting.getSongZwischenspeicher().size(),
								tfdTitelname.getText(), tfdBandname.getText(), tfdGenre.getText()));
						System.out.println("Song hinzugefügt!");
					} else {
						for (int i = 0; i <musikvoting.getSongZwischenspeicher().size(); i++) {
							if (musikvoting.getSongZwischenspeicher().get(i).getTitel().equals(tfdTitelname.getText())
									&& musikvoting.getSongZwischenspeicher().get(i).getInterpret().equals(tfdBandname.getText())) {
								System.out.println("Der Song exisitert bereits!");
								break;
							} else {
								musikvoting.addSong(new Song(musikvoting.getSongZwischenspeicher().size(),
										tfdTitelname.getText(), tfdBandname.getText(), tfdGenre.getText()));
								System.out.println("Song hinzugefügt!");
								break;
							}
						}
					}

					tfdBandname.setText("");
					tfdTitelname.setText("");
					tfdGenre.setText("");
				}
			}
		});
		btnMusiktitelHinzufgen.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		contentPane.add(btnMusiktitelHinzufgen);
	}

}
