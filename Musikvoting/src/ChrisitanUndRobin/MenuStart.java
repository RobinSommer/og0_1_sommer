package ChrisitanUndRobin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.Dimension;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MenuStart extends JFrame
{

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MenuStart frame = new MenuStart();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuStart()
	{
		setTitle("Musikvoting - IMTMusic");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 872, 597);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		Musikvoting master = new Musikvoting();
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(20);
		panel.setPreferredSize(new Dimension(10, 150));
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Musikvoting - Hauptmen\u00FC");
		lblNewLabel.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(0, 0, 0)));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 60, 5));
		
		JButton btnNewButton = new JButton("Songs hinzuf\u00FCgen");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				try {
					MusikEintragenGUI frame = new MusikEintragenGUI();
					frame.setVisible(true);
				} catch (Exception g) {
					g.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setForeground(new Color(0, 100, 0));
		btnNewButton.setPreferredSize(new Dimension(200, 125));
		panel_1.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Voten");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				try
				{
					MusikVotenGUI frame = new MusikVotenGUI();
					frame.setVisible(true);
				}
				catch (Exception g)
				{
					g.printStackTrace();
				}
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setForeground(new Color(75, 0, 130));
		btnNewButton_1.setPreferredSize(new Dimension(200, 125));
		panel_1.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Playlist abrufen");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				try {
					PlayList frame = new PlayList();
					frame.setVisible(true);
				} catch (Exception g) {
					g.printStackTrace();
				}
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton_2.setBackground(Color.WHITE);
		btnNewButton_2.setForeground(new Color(255, 215, 0));
		btnNewButton_2.setPreferredSize(new Dimension(200, 125));
		panel_1.add(btnNewButton_2);
		
		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setHgap(30);
		panel_2.setPreferredSize(new Dimension(10, 100));
		contentPane.add(panel_2, BorderLayout.SOUTH);
		
		JButton btnNewButton_3 = new JButton("Alle Inhalte l\u00F6schen");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				master.deletePlaylist();
				
			}
		});
		btnNewButton_3.setBackground(Color.LIGHT_GRAY);
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton_3.setForeground(Color.RED);
		btnNewButton_3.setPreferredSize(new Dimension(200, 50));
		panel_2.add(btnNewButton_3);
		
		JLabel lblNewLabel_1 = new JLabel("Von: Robin, Christian und Samer");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		panel_2.add(lblNewLabel_1);
	}

}
