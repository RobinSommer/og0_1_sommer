package ChrisitanUndRobin;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PlayList extends JFrame {

	private static final long serialVersionUID = -3139007621794736215L;
	private JPanel contentPane;
	private JTable table;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlayList frame = new PlayList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public PlayList() {
		setTitle("Musikvoting - Playlist abrufen");
		setBounds(100, 100, 450, 900);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JLabel lblPartyplaylistDerGruppe = new JLabel("Partyplaylist");
		lblPartyplaylistDerGruppe.setHorizontalAlignment(SwingConstants.CENTER);
		lblPartyplaylistDerGruppe.setFont(new Font("Comic Sans MS", Font.BOLD, 20));
		lblPartyplaylistDerGruppe.setForeground(new Color(255, 255, 255));
		contentPane.add(lblPartyplaylistDerGruppe, BorderLayout.NORTH);
		
		Musikvoting mv = new Musikvoting();
		
		String[] spaltennamen = {"Anzahl Votes", 
				"Titel",
                "Interpret",
                "Genre"};
		String[][] data = (mv.getSongsInTDAWithVotes());
		table = new JTable(data, spaltennamen);
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		contentPane.add(scrollPane, BorderLayout.CENTER);
	}

}
