package ChrisitanUndRobin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class MusikVotenGUI extends JFrame
{

	private static final long serialVersionUID = 4510828557344012125L;
	private JPanel contentPane;
	private JTextField tfdLoginname;
	private JTable tableMusiktitel;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					MusikVotenGUI frame = new MusikVotenGUI();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public MusikVotenGUI()
	{
		setTitle("Musikvoting - Voting");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel pnlNorth = new JPanel();
		pnlNorth.setBackground(Color.GRAY);
		contentPane.add(pnlNorth, BorderLayout.NORTH);
		pnlNorth.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblLoginname = new JLabel("Loginname:");
		lblLoginname.setForeground(Color.WHITE);
		lblLoginname.setHorizontalAlignment(SwingConstants.LEFT);
		lblLoginname.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		pnlNorth.add(lblLoginname);

		tfdLoginname = new JTextField();
		pnlNorth.add(tfdLoginname);
		tfdLoginname.setColumns(10);

		Musikvoting mv = new Musikvoting();

		JButton btnVoten = new JButton("Voten");
		btnVoten.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if (tfdLoginname.getText().equals(""))
					tfdLoginname.setBackground(Color.RED);
				else
				{
					tfdLoginname.setBackground(Color.WHITE);
					String loginname = tfdLoginname.getText();
					int index = tableMusiktitel.getSelectedRow();
					/*Song song = new Song(0, (String) tableMusiktitel.getValueAt(index, 0),
							(String) tableMusiktitel.getValueAt(index, 1),
							(String) tableMusiktitel.getValueAt(index, 2)); */

					Song song = null;
					mv.loadSongs();
					for (int i = 0; i < mv.getSongZwischenspeicher().size(); i++)
					{
						if (mv.getSongZwischenspeicher().get(i).getTitel()
								.equals((String) tableMusiktitel.getValueAt(index, 0))
								&& mv.getSongZwischenspeicher().get(i).getInterpret()
										.equals((String) tableMusiktitel.getValueAt(index, 1)))
						{
							song = mv.getSongZwischenspeicher().get(i);
						}
					}

					mv.voteEintragen(loginname, song);
				}
			}
		});
		contentPane.add(btnVoten, BorderLayout.EAST);

		String[] spaltennamen = { "Bandname", "Titelname", "Genre" };
		String[][] data = mv.getSongsInTDA();
		tableMusiktitel = new JTable(data, spaltennamen);
		tableMusiktitel.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

		JScrollPane scrollPane = new JScrollPane(tableMusiktitel);
		tableMusiktitel.setFillsViewportHeight(true);
		contentPane.add(scrollPane, BorderLayout.CENTER);
	}
}
