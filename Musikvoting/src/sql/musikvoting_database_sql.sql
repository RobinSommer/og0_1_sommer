DROP DATABASE if EXISTS musikvoting;
CREATE DATABASE musikvoting;
USE musikvoting;

CREATE TABLE T_Gast (
  Benutzername VARCHAR(59) PRIMARY KEY 
)
ENGINE = INNODB;

CREATE TABLE T_Musik_ID (
  Musik_ID VARCHAR(59) PRIMARY KEY,
  Titel VARCHAR(60),
  Interpret VARCHAR(60),
  Genre VARCHAR(60)
)
ENGINE = InnoDB;

CREATE TABLE T_Musik_T_G�ste (
   VoteID VARCHAR(59) PRIMARY KEY,
   Benutzername VARCHAR(59) NOT NULL,
   Musik_ID VARCHAR(59) NOT NULL,
   
   CONSTRAINT benutzer1 FOREIGN KEY (Benutzername) REFERENCES t_gast (Benutzername) ON DELETE CASCADE,
   CONSTRAINT musikid1 FOREIGN KEY (Musik_ID) REFERENCES t_musik_id (Musik_ID) ON DELETE CASCADE
)
ENGINE = INNODB;

ALTER TABLE T_Gast
ADD Adelsgeschlecht VARCHAR(20),
ADD Ritter VARCHAR(5);