package de.futurehome.tanksimulator;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.beans.*;
import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
@SuppressWarnings("serial")
public class TankSimulator extends Frame {
	
	public Tank myTank;
	
	private Label lblUeberschrift = new Label("Tank-Simulator");
	public  Label lblFuellstand = new Label("        ");
	public  Label lblProzent = new Label("");
	
	public Button btnBeenden = new Button("Beenden");
	public Button btnEinfuellen = new Button("Einfüllen");
	public Button btnVerbrauchen = new Button("Verbrauchen");
	public Button btnReset = new Button("Zurücksetzten");
	
	private Panel pnlNorth = new Panel();
	private Panel pnlCenter = new Panel();
	private Panel pnlSouth = new Panel(new GridLayout(1, 0));
	
	// Progress Bar
	public JProgressBar progressBar;
	
	// Slider
	JSlider slider = new JSlider();

	private MyActionListener myActionListener = new MyActionListener(this);

	public TankSimulator() {
		super("Tank-Simulator");
		
		myTank = new Tank(0);
		
		this.lblUeberschrift.setFont(new Font("", Font.BOLD, 16));
		
		this.pnlNorth.add(this.lblUeberschrift);
		
		// Slider
		slider.setMinimum(1);
		slider.setMaximum(4);
		slider.setMajorTickSpacing(1);
		slider.setMinorTickSpacing(1);
		
		slider.createStandardLabels(1); 
		slider.createStandardLabels(2);
		slider.createStandardLabels(3);
		slider.createStandardLabels(4);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setValue(1);
		
		slider.addChangeListener(new ChangeListener() {
		      public void stateChanged(ChangeEvent event) {
		        myTank.verbrauch = slider.getValue();
		      }
		    });
		
		// Progress Bar
		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		
		pnlCenter.setLayout(null);
		this.pnlCenter.add(this.lblFuellstand);
		this.pnlCenter.add(this.lblProzent);
		this.pnlCenter.add(progressBar);
		this.pnlCenter.add(slider);
		
		this.pnlSouth.add(this.btnEinfuellen);
		this.pnlSouth.add(this.btnVerbrauchen);
		this.pnlSouth.add(this.btnReset);
		this.pnlSouth.add(this.btnBeenden);
		
		lblFuellstand.setBounds(130, 5, 50, 20);
		lblProzent.setBounds(225, 5, 50, 20);
		progressBar.setBounds(118, 45, 150, 20);
		slider.setBounds(118, 85, 150, 50);
		
		this.add(this.pnlNorth, BorderLayout.NORTH);
		this.add(this.pnlCenter, BorderLayout.CENTER);
		this.add(this.pnlSouth, BorderLayout.SOUTH);
		
		this.pack();
		this.setVisible(true);
		
		// Ereignissteuerung
		this.btnEinfuellen.addActionListener(myActionListener);
		this.btnVerbrauchen.addActionListener(myActionListener);
		this.btnBeenden.addActionListener(myActionListener);
		this.btnReset.addActionListener(myActionListener);
		
		this.setResizable(false);
		this.setSize(400, 250);
	}

	public static void main(String argv[]) {
		TankSimulator f = new TankSimulator();
		
		f.lblFuellstand.setText(0 + " Liter");
		f.lblProzent.setText(0 + "%");
		
		log("----- Programm gestartet! -----");
	}
	
	public static void log(String infos)
	{
		Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
        logger.setLevel(Level.INFO);

        logger.info(infos + "\n");
	}
}