package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden) {
			f.log("----- Programm beendet! -----");
			System.exit(0);
		}

		if (obj == f.btnEinfuellen) {
			int fuellstand = f.myTank.getFuellstand();
			int fuel = fuellstand;

			if (fuellstand <= 95) {
				fuellstand = fuellstand + 5;
				f.myTank.setFuellstand(fuellstand);
				f.progressBar.setValue(fuellstand);
			} else if (fuellstand > 95) {
				fuellstand = 100;
				f.myTank.setFuellstand(fuellstand);
				f.progressBar.setValue(fuellstand);
			}

			f.log("Tank aufgef�llt um: " + (fuellstand - fuel) + ". Die F�llmenge betr�gt nun: " + fuellstand);

			f.lblFuellstand.setText(fuellstand + " Liter");
			f.lblProzent.setText(fuellstand + "%");
		}

		if (obj == f.btnVerbrauchen) {
			int fuellstand = f.myTank.getFuellstand();
			int fuel = fuellstand;

			if (fuellstand >= f.myTank.verbrauch) {
				fuellstand = fuellstand - f.myTank.verbrauch;
				f.myTank.setFuellstand(fuellstand);
				f.progressBar.setValue(fuellstand);

			} else if (fuellstand < f.myTank.verbrauch) {
				fuellstand = 0;
				f.myTank.setFuellstand(fuellstand);
				f.progressBar.setValue(fuellstand);
			}

			f.log("Tank geleert um: " + (fuel - fuellstand) + ". Die F�llmenge betr�gt nun: " + fuellstand);

			f.lblFuellstand.setText(fuellstand + " Liter");
			f.lblProzent.setText(fuellstand + "%");
		}

		if (obj == f.btnReset) {
			int fuellstand = f.myTank.getFuellstand();
			int fuel = fuellstand;

			fuellstand = 0;
			f.myTank.setFuellstand(fuellstand);

			f.progressBar.setValue(fuellstand);

			f.log("--- Tank geleert! ---");

			f.lblFuellstand.setText(fuellstand + " Liter");
			f.lblProzent.setText(fuellstand + "%");
		}
	}
}