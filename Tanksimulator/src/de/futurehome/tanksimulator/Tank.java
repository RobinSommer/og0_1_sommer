package de.futurehome.tanksimulator;
public class Tank {
	
	private int fuellstand;
	public int verbrauch = 1;

	public Tank(int fuellstand) {
		this.fuellstand = fuellstand;
	}

	public int getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(int fuellstand) {
		this.fuellstand = fuellstand;
	}

}
