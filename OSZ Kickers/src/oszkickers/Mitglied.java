package oszkickers;

public class Mitglied 
{
	// Variables
	protected String name;
	protected String telefonnummer;
	protected boolean jahresbeitrag;
	
	// Constructor
	public Mitglied(String name_new, String tel_new, boolean jahr_new)
	{
		name = name_new;
		telefonnummer = tel_new;
		jahresbeitrag = jahr_new;
	}

	// Methods
	public String getName() 
	{
		return name;
	}

	public void setName(String name_new) 
	{
		this.name = name_new;
	}

	public String getTelefonnummer() 
	{
		return telefonnummer;
	}

	public void setTelefonnummer(String tel_new) 
	{
		this.telefonnummer = tel_new;
	}

	public boolean getJahresbeitrag() 
	{
		return jahresbeitrag;
	}

	public void setJahresbeitrag(boolean jahr_new) 
	{
		this.jahresbeitrag = jahr_new;
	}
}
