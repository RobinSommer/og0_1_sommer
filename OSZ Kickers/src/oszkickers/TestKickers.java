package oszkickers;

public class TestKickers
{
	public static void main(String[] args)
	{
		// Initialisierung
		Spieler spieler01 = new Spieler("Tim", "01761221235", false, 9, "St�rmer");
		
		Trainer trainer01 = new Trainer("Matthias", "0174334421", true, 'B', 300.99);
		
		Schiedsrichter schiedsrichter01 = new Schiedsrichter("Gustav", "01665544332", true, 122);
		
		Mannschaftsleiter mannschaftsleiter01 = new Mannschaftsleiter("Andreas", "0156332413", true, 30.22);
		
		// Ausgabe in der Konsole
		System.out.println("--- Mitgliederverwaltung ---");
		System.out.println("");
		
		System.out.println("- Spieler - ");
		System.out.println("Name: " + spieler01.getName());
		System.out.println("Telefonnummer: " + spieler01.getTelefonnummer());
		System.out.println("Jahresbeitrag bezahlt?: " + spieler01.getJahresbeitrag());
		System.out.println("Trikonummer: " + spieler01.getTrikonummer());
		System.out.println("Spielposition: " + spieler01.getSpielposition());
		
		System.out.println("");
		System.out.println("");
		
		System.out.println("- Trainer - ");
		System.out.println("Name: " + trainer01.getName());
		System.out.println("Telefonnummer: " + trainer01.getTelefonnummer());
		System.out.println("Jahresbeitrag bezahlt?: " + trainer01.getJahresbeitrag());
		System.out.println("Lizensklasse: " + trainer01.getLizensklasse());
		System.out.println("Aufwandentsch�digung: " + trainer01.getAufwandentschaedigung() + "�");
		
		System.out.println("");
		System.out.println("");
		
		System.out.println("- Schiedsrichter - ");
		System.out.println("Name: " + schiedsrichter01.getName());
		System.out.println("Telefonnummer: " + schiedsrichter01.getTelefonnummer());
		System.out.println("Jahresbeitrag bezahlt?: " + schiedsrichter01.getJahresbeitrag());
		System.out.println("Gepfiffene Spieler: " + schiedsrichter01.getGepfiffeneSpieler());
		
		System.out.println("");
		System.out.println("");
		
		System.out.println("- Mannschaftsleiter - ");
		System.out.println("Name: " + mannschaftsleiter01.getName());
		System.out.println("Telefonnummer: " + mannschaftsleiter01.getTelefonnummer());
		System.out.println("Jahresbeitrag bezahlt?: " + mannschaftsleiter01.getJahresbeitrag());
		System.out.println("Rabatt: " + mannschaftsleiter01.getRabatt() + "%");
	}
}
