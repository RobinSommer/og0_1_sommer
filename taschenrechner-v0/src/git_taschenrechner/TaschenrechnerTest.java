package git_taschenrechner;

import java.util.Scanner;

public class TaschenrechnerTest {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue = 0;
		int num1 = 0;
		int num2 = 0;

		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Dividieren     |");
		System.out.println("|        4. Multiplizieren |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print("Select option: ");

		swValue = myScanner.next().charAt(0);

		if (swValue < '5' && swValue > '0') {

			System.out.println("");
			System.out.println("- Choose Numbers -");
			System.out.print("Number 1: ");

			num1 = myScanner.nextInt();

			System.out.print("Number 2: ");

			num2 = myScanner.nextInt();
			
			System.out.println("");

			// Switch construct
			switch (swValue) {
			case '1':
				System.out.println("Result:" + ts.add(num1, num2));
				break;

			case '2':
				System.out.println("Result:" + ts.sub(num1, num2));
				break;

			case '3':
				System.out.println("Result:" + ts.div(num1, num2));
				break;

			case '4':
				System.out.println("Result: " + ts.mul(num1, num2));
				break;

			default:
				System.out.println("Invalid selection");
				break;
			}
			myScanner.close();
		}
		else 
		{
			for (int i = 0; i < 50; ++i) System.out.println();
		}
	}
}
