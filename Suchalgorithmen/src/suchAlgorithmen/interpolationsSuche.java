package suchAlgorithmen;

public class interpolationsSuche
{
	public long[] interpolation(long[] liste, int suchendeZahl)
	{
		long versuche = 0;
		long linkeSeite = 0;
		long rechteSeite = liste.length - 1;
	
		while (suchendeZahl >= liste[(int) linkeSeite] && suchendeZahl <= liste[(int) rechteSeite])
		{
			long anzVerschElemente = liste[(int) rechteSeite] - liste[(int) linkeSeite];
			
			long position = linkeSeite + (long)(((double)rechteSeite - linkeSeite) * 
					(suchendeZahl - liste[(int) linkeSeite]) / anzVerschElemente);
			
			if (suchendeZahl > liste[(int) position])
			{
				linkeSeite = position + 1;
				versuche++;
			}
			else if (suchendeZahl < liste[(int) position])
			{
				rechteSeite = position - 1;
				versuche++;
			}
			else
			{
				long[] gefunden = {position, versuche};
				return gefunden;
			}
		}
		
		long[] nichtGefunden = {-1, versuche};
		return nichtGefunden;
	}
}
