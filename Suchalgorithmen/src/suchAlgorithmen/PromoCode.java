package suchAlgorithmen;

public class PromoCode
{
	public static int findPromoCode(long[] list, long searchValue)
	{
		long links = 0, rechts = list.length - 1;

		while (searchValue >= list[(int) links] && searchValue <= list[(int) rechts])
		{
			long anzElemente = list[(int) rechts] - list[(int) links];
			long pos = links
					+ (long) (((double) rechts - links) * (searchValue - list[(int) links]) / anzElemente);

			if (searchValue > list[(int) pos])
			{
				links = pos + 1;
			}
			else if (searchValue < list[(int) pos])
			{
				rechts = pos - 1;
			}
			else
			{
				return (int) pos;
			}
		}
		return -1;
	}
}
