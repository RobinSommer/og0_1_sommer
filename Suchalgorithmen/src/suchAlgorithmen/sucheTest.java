package suchAlgorithmen;

import java.util.Random;
import java.util.Scanner;

public class sucheTest
{
	static binaereSuche binaer = new binaereSuche();
	static interpolationsSuche interpolation = new interpolationsSuche();
	
	
	public static void main(String Args[])
	{
		while (true)
		{
			System.out.print("Wie viele Zahlen soll die Liste enthalten?: ");
			
			Scanner scan = new Scanner(System.in);
			int plaetze = scan.nextInt();
			System.out.print("Nach welcher Zahl soll die Liste durchsucht werden?: " );
			
			int suchendeZahl = scan.nextInt();
			
			System.out.print("Wollen Sie die Bin�re- oder Interpolationssuche verwenden? [i/b]: ");
			
			String suchart = scan.next();
			
			long[] liste = listeErzeugen(plaetze);

			
			long ergebnis[] = new long[2];
			double time = System.currentTimeMillis();
			
			if (suchart.equals("i"))
			{
				ergebnis = interpolation.interpolation(liste, suchendeZahl);
			}
			else if (suchart.equals("b"))
			{
				ergebnis = binaer.binaer(liste, suchendeZahl);
			}
			System.out.println("");
			
			if (ergebnis[0] == -1)
			{
				System.out.println("Die Zahl befindet sich nicht in dem Array!");
			}
			else
			{
				System.out.println("Der Index der gesuchten Zahl ist: " + ergebnis[0] + " ---> Die Zahl an der Stelle ist: " + liste[(int) ergebnis[0]]);
			}
			System.out.println("Die Versuche des Suchalgorithmus waren: " + ergebnis[1]);
			System.out.println("Die Zeit daf�r betr�gt: " + (System.currentTimeMillis() - time) + "ms");
			
			System.out.println("");
			System.out.println("-------------------------------------------------------------------------------------");
			System.out.println("");
		}
	}
	
	public static long[] listeErzeugen(int anzZahlen)
	{
		Random ran = new Random(111111);
		long[] liste = new long[anzZahlen];
		long nextZahl = 0;
		
		for (int i = 0; i < anzZahlen; i++)
		{
			nextZahl += ran.nextInt(5) + 1;
			liste[i] = nextZahl;
		}

		return liste;
	}
}

