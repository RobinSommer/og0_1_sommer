package suchAlgorithmen;

public class binaereSuche
{
	public long[] binaer(long[] liste, long suchendeZahl)
	{
		long versuche = 0;
		long linkeSeite = 0;
		long rechteSeite = liste.length - 1;

		while (linkeSeite <= rechteSeite)
		{
			long mittlererWert = linkeSeite + ((rechteSeite - linkeSeite) / 2);

			versuche++;
			if (suchendeZahl == liste[(int) mittlererWert])
			{
				long[] gefunden = {mittlererWert, versuche};
				return gefunden;
			}
			else
			{
				versuche++;
				if (liste[(int) mittlererWert] > suchendeZahl)
				{
					rechteSeite = mittlererWert - 1;
					
				}
				else
				{
					linkeSeite = mittlererWert + 1;
				}
			}
			
		}
		
		long[] nichtGefunden = {-1, versuche};
		return nichtGefunden;
	}
}
