package sortier_verfahren;

import java.util.*;

public class MergeSort {

	static int counter = 0;
	static int[] zahlen = new int[20];

	public static int[] sortieren(int zahl1, int zahl2) {
		if (zahl1 < zahl2) {
			int q = (zahl1 + zahl2) / 2;
			counter++;
			sortieren(zahl1, q);
			sortieren(q + 1, zahl2);
			merge(zahl1, q, zahl2);
		}
		return zahlen;
	}

	private static void merge(int zahl3, int zahl4, int zahl5) {
		int[] arr = new int[zahlen.length];
		int i, j;
		for (i = zahl3; i <= zahl4; i++) {
			arr[i] = zahlen[i];
		}
		for (j = zahl4 + 1; j <= zahl5; j++) {
			arr[zahl5 + zahl4 + 1 - j] = zahlen[j];
		}
		i = zahl3;
		j = zahl5;
		for (int k = zahl3; k <= zahl5; k++) {
			if (arr[i] <= arr[j]) {
				zahlen[k] = arr[i];
				counter++;
				i++;
			} else {
				zahlen[k] = arr[j];
				counter++;
				j--;
			}
		}

		System.out.print("{");
		for (int d = 0; d < zahlen.length; d++) {
			
			if (zahlen[d] != 0) {
				if (d == zahlen.length - 1) {
					System.out.print(zahlen[d]);
				} else {
					System.out.print(zahlen[d] + ", ");
				}
				
			}
		}
		System.out.print("}");
		System.out.println("");
		
	}

	public static void main(String[] args) {

		// Werteeingabe
		System.out.println("--- Mergesort - Sortieralgorithmus ---");
		System.out.println("");
		System.out.print("Wollen Sie eigenen Zahl eintippen oder Zufallszahlen generieren lassen? [1/2]: ");

		Scanner scan = new Scanner(System.in);
		int eingabe = scan.nextInt();

		switch (eingabe) {
		case 1:
			System.out.println("");
			System.out.println("Bitte geben Sie nun 20 verschiedene Zahlen an!");
			System.out.println("");
			for (int i = 0; i < zahlen.length; i++) {
				System.out.print("Geben Sie bitte die " + (i + 1) + ". Zahl ein: ");
				zahlen[i] = scan.nextInt();
			}
			System.out.println("");
			break;
		case 2:
			Random ran = new Random();
			for (int i = 0; i < zahlen.length; i++) {
				zahlen[i] = ran.nextInt(100) + 1;
			}
			break;
		default:
			System.out.println("Error: Falsche Eingabe!");
			break;
		}

		System.out.println("");
		System.out.println("Unsortiert:");
		for (int i = 0; i < zahlen.length; i++) {
			if (i == zahlen.length - 1) {
				System.out.print(zahlen[i]);
			} else {
				System.out.print(zahlen[i] + ", ");
			}
		}

		System.out.println("");
		System.out.println("");

		int[] arr = sortieren(0, zahlen.length - 1);

		System.out.println("");
		System.out.println("Sortiert:");
		// Ausgabe
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				System.out.print(arr[i]);
			} else {
				System.out.print(arr[i] + ", ");
			}
		}
		
		System.out.println("");
		System.out.println("");
		System.out.println("Vergleiche: " + counter);
		System.out.println("");
		System.out.println("Robin Sommer");
		System.out.println("Can Altun");
	}
}