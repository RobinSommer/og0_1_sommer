package primzahl;

import java.util.*;

public class Primzahl
{
	static long zahl;

	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		while (true)
		{
			System.out.println("--- Primzahl-Pr�fer ---");
			System.out.println("");
			System.out.print("Geben Sie eine Zahl ein, die Sie pr�fen wollen: ");

			zahl = scan.nextLong();
			System.out.println("");
			System.out.println("--------------------------------------------");
			System.out.println("");

			System.out.println("Deine Zahl ist eine Primzahl? " + isPrimzahl(zahl));

			System.out.println("");
			System.out.println("--------------------------------------------");
			System.out.println("");
		}
	}

	public static boolean isPrimzahl(long zahl)
	{
		long time = System.currentTimeMillis();

		boolean isPrimzahl = true;

		for (long i = 2; i < zahl; i++)
		{
			int modulo = (int) (zahl % i);
			if (modulo == 0)
			{
				isPrimzahl = false;
				break;
			}
		}
		System.out.println("Das Ausrechnen hat " + (System.currentTimeMillis() - time) + "ms gedauert");
		System.out.println("");
		return isPrimzahl;
	}
}
