package addon;

public class Addon 
{
	private int idNummer;
	private double verkaufspreis;
	private String bezeichnung;
	private String art;
	private int anzahl;
	private int maxAnzahl;

	// Constructor
	public Addon() {}
	
	public Addon(int id, double preis, String bez, String artneu, int anz, int maxAnz) 
	{
		idNummer = id;
		verkaufspreis = preis;
		bezeichnung = bez;
		art = artneu;
		anzahl = anz;
		maxAnzahl = maxAnz;
	}
	
	// Methods
	public void aendereBestand(int bestand)
	{
		anzahl = bestand;
	}
	
	public double gesamtwertAddons()
	{
		double ges = (anzahl*verkaufspreis);
		return ges;
	}
	
	// Setter
	public void setIdNummer(int id) {idNummer = id;}
	
	public void setVerkaufspreis(double preis) {verkaufspreis = preis;}
	
	public void setBezeichnung(String bez) {bezeichnung = bez;}
	
	public void setArt(String artNeu) {art = artNeu;}
	
	public void setAnzahl(int anz) {anzahl = anz;}
	
	public void setMaxAnzahl(int maxAnz) {maxAnzahl = maxAnz;}
	
	// Getter
	public int getIdNummer() {return idNummer;}
	
	public double getVerkaufspreis() {return verkaufspreis;}
	
	public String getBezeichnung() {return bezeichnung;}
	
	public String getArt() {return art;}
	
	public int getAnzahl() {return anzahl;}
	
	public int getMaxAnzahl() {return maxAnzahl;}
}
