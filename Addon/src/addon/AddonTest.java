package addon;

import java.util.Scanner;

public class AddonTest {

	private static int id;
	private static double preis;
	private static String bez;
	private static String art;
	private static int anz;
	private static int maxAnz;

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("--- Addon-Creator ---");
		System.out.println("");
		System.out.println("Willkommen zum Addon-Creator!");
		System.out.println("Sie k�nnen nun damit beginnen ein Addon zu entwickeln.");
		System.out.println("");
		System.out.print("Beginnen Sie mit den Namen des Addons: ");

		bez = scan.next();

		System.out.println("");
		System.out.print("Nun legen Sie die Art fest [Tierfutter/Attraktion]: ");

		art = scan.next();

		System.out.println("");
		System.out.print("Als n�chtes legen Sie fest, wie oft man ihr Addon kaufen kann [Ganzzahl]: ");

		maxAnz = scan.nextInt();

		System.out.println("");
		System.out.print("Bestimmen Sie nun eine ID f�r ihr Addon [6 stellige Ganzzahl]: ");

		id = scan.nextInt();

		System.out.println("");
		System.out.print("Zuletzt bestimmen Sie bitte einen Preis f�r ihr Addon [0-2,99]: ");

		preis = scan.nextDouble();

		System.out.println("");
		System.out.println("---------------------------------------------------------------------");
		System.out.println("");

		Addon addon1 = new Addon(id, preis, bez, art, 0, maxAnz);

		System.out.println("Ihr Addon wurde erstellt!:");
		System.out.println("");

		System.out.println("Name: " + addon1.getBezeichnung());
		System.out.println("Art: " + addon1.getArt());
		System.out.println("Maximale Kaufanzahl: " + addon1.getMaxAnzahl());
		System.out.println("Addon ID: " + addon1.getIdNummer());
		System.out.println("Verkaufspreis: " + addon1.getVerkaufspreis());
	}

}
